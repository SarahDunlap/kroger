const initialState = {};

const cart = (state = initialState, action) => {
  const { type, upc } = action;
  switch(type) {
    case 'ADD_PRODUCT_TO_CART':
      return {
        ...state,
        [upc]: {
          quantity: state[upc] ? state[upc].quantity + 1 : 1,
          upc: upc
        }
      }
    default:
      return state;
  }
};

// cart selectors
export const getCartUpcs = state => Object.keys(state.cart);
export const getItemQuantity = (state, upc) => state.cart[upc].quantity;

export default cart;
