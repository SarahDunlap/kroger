import React from 'react';

import './ProductCard.css';

const ProductCard = ({ name, image, price, upc, addToCart }) => {
  return (
    <div className='ProductCard'>
      <img src={image}/>
      <div className='ProductCard-details'>
        <div>{ name }</div>
        <div>${ price.toFixed(2) }</div>
      </div>
      <button onClick={() => addToCart(upc)}>Add To Cart</button>
    </div>
  );
};

export default ProductCard;
