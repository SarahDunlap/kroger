const initialState = [];

const products = (state = initialState, action) => {
  switch(action.type) {
    default:
      return state;
  }
};

// product selectors
export const getProducts = state => state.products;
export const getProduct = (state, upc) => state.products.find(product => product.upc === upc);

export default products;
