import React from 'react';
import { connect } from 'react-redux';
import { getItemQuantity } from '../reducers/cart';
import { getProduct } from '../reducers/products';

import './CartItem.css';

const CartItem = ({ upc, quantity, price, name, image }) => {
  return (
    <div className='CartItem'>
      <img src={image}/>
      <div className='CartItem-details'>
        <div>
          <div>{name}</div>
          <div>${price.toFixed(2)}</div>
        </div>
        <div>Quantity: {quantity}</div>
      </div>
    </div>
  );
};

const mapStateToProps = (state, props) => ({
  quantity: getItemQuantity(state, props.upc),
  ...getProduct(state, props.upc)
});

export default connect(mapStateToProps)(CartItem);
