export const addProductToCart = (upc) => ({
  type: 'ADD_PRODUCT_TO_CART',
  upc
});
