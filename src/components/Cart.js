import React from 'react';
import { connect } from 'react-redux';
import { getCartUpcs } from '../reducers/cart';
import CartItem from './CartItem';

import './Cart.css';

const Cart = ({ upcs }) => {
  return (
    <div className='Cart'>
      <h1>Cart:</h1>
      { upcs.map(upc => <CartItem upc={upc} />) }
    </div>
  );
};

const mapStateToProps = (state, props) => ({
  upcs: getCartUpcs(state)
});

export default connect(mapStateToProps)(Cart);
