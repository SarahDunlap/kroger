var fastify = require('fastify')();
var firebase = require('firebase');
var cors = require('cors');

var firebaseConfig = {
  apiKey: 'AIzaSyAO_AyPEFJzMoiw7qLrpvVi_O7mrVdsjFE',
  authDomain: 'krogercodetest.firebaseapp.com',
  databaseURL: 'https://krogercodetest.firebaseio.com',
  projectId: 'krogercodetest',
  storageBucket: '',
  messagingSenderId: '745799947067'
}

firebase.initializeApp(firebaseConfig)

var db = firebase.database()

// If you need to change the products in the db, uncomment this and run the server

// var prods = [{
//   name: 'Bananas',
//   upc: '1',
//   image: 'https://www.kroger.com/product/images/medium/front/0000000004011',
//   price: 4.00
// }, {
//   name: 'Banana Yogurt',
//   upc: '2',
//   image: 'https://www.kroger.com/product/images/medium/front/0007047000313',
//   price: 1.25
// }, {
//   name: 'Grapes',
//   upc: '3',
//   image: 'https://www.kroger.com/product/images/medium/front/0000000004023',
//   price: 5.00
// },
// {
//   name: 'Grape Jelly',
//   upc: '4',
//   image: 'https://www.kroger.com/product/images/medium/front/0004180050150',
//   price: 6.00
// },
// {
//   name: 'Grape Juice',
//   upc: '5',
//   image: 'https://www.kroger.com/product/images/medium/front/0004180020750',
//   price: 4.50
// }, {
//   name: 'White Bread',
//   upc: '6',
//   image: 'https://www.kroger.com/product/images/medium/front/0007130104737',
//   price: 2.25
// }, {
//   name: 'Wheat Bread',
//   upc: '7',
//   image: 'https://www.kroger.com/product/images/medium/front/0007225003706',
//   price: 2.25
// }, {
//   name: 'Rye Bread',
//   upc: '8',
//   image: 'https://www.kroger.com/product/images/medium/front/0005172100109',
//   price: 2.25
// }, {
//   name: 'French Bread',
//   upc: '9',
//   image: 'https://www.kroger.com/product/images/medium/left/0004157322506',
//   price: 2.50
// }];
//
// prods.forEach(product => db.ref('products/' + product.upc).set(product));

fastify.use(cors())

fastify.get('/products', async (request, reply) => {
  const { search } = request.query
  var productRef = db.ref('products/')

  productRef.on('value', function (snapshot) {
    if (search) {
      const filteredProducts = snapshot.val().filter(product => {
        return product.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
      })
      reply.send(filteredProducts)
    }

    reply.send(snapshot.val())
  })
});

fastify.listen(4000, 'localhost', err => {
  if (err) throw error
  console.log(`Server listening on ${fastify.server.address().port}`)
})
