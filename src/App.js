import React, { Component } from 'react';
import logo from './logo.svg';
import { fetchProducts } from './api';
import ProductGrid from './components/ProductGrid';
import Cart from './components/Cart';

import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      products: []
    };
  }

  render() {
    return (
      <div className="Kroger">
        <header></header>
        <div className='Kroger-content'>
          <div className='Kroger-shop'>
            <ProductGrid/>
          </div>
          <div className='Kroger-cart'>
            <Cart/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
