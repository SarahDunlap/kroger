# Kroger Cart Code Test

## Setup
To install:

```
git clone https://github.com/hedgerh/kroger-code-test.git
cd kroger-code-test
yarn
```

Then, in one console, start the server:

```
yarn run start:server
```

This will start up the API that connects to a firebase db to fetch products.

In another console, start the client:

```
yarn start
```

## Overview

This code challenge will test your ability to implement features using React and Redux.

For this challenge, you will take a simple shopping cart and implement some features using Redux.

The challenges are:

1. Fetch and display products

2. Setup a product search bar

3. Show the total price of all items in the shopping cart

Don't get hung up on styling things.  We're just worried about the functionality.

If anything isn't working as you expect, or you have any questions, feel free to reach out.  You can open an issue on the repo if you'd like.

The Redux documentation is a great resource for getting started with Redux.  https://redux.js.org/

### Challenge 1. Fetch and display all products

The first challenge is to fetch all of the products from the API and display them in the `ProductGrid` component.

You need to fetch the products when the `ProductGrid` component mounts.  This should be done by triggering a `fetchProducts` action.  The products should then be added to the `products` state by using the `products` reducer.

A `ProductCard` component has been provided for displaying products in the grid.  Make sure you set it up so that products are added to the cart when clicking the "Add To Cart" button.

Notice that each time you hit `Add To Cart`, it'll increment the quantity of that item.

The API has a GET endpoint:

`GET /products`

Fetches all products from the Firebase database.

*Query params:*
- `search` - a search query for finding products. Case insensitive.

*Example Response:*
```
{
  upc: 1,
  name: 'Bananas',
  price: 4.00,
  image: 'https://www.kroger.com/product/images/medium/front/0000000004011'
}
```

You will use redux-thunk to do asynchronous things in your redux actions:

https://github.com/gaearon/redux-thunk#motivation

It's already setup for you to use.  There's no installation needed.

### Challenge 2. Setup a product search bar
Now that we're able to display products, let's use the `/products` endpoint's search query param to set up a product search bar.

Add it to the header component within the `App` component. When a user enters a search query and hits "Search", it should fetch the products matching the search query.  Remember that the query param is case insensitive.

Example:  If a user types in 'bread', it should return White Bread, Wheat Bread, Rye Bread, and French Bread.

### Challenge 3. Show the total price of products in the cart

Now we'd like to calculate the total price of all items in the cart.  You can add this wherever you want in the `Cart` component.  Remember to not get hung up on the styling.

Hint: you'll want to do this with a selector in `Cart`'s `mapStateToProps` function.  Check out `CartItem`'s use of selectors for hints, as well.
