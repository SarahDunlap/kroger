import React from 'react';
import { connect } from 'react-redux';
import { getProducts } from '../reducers/products';
import { fetchProducts } from '../actions/products';
import { addProductToCart } from '../actions/cart';
import ProductCard from './ProductCard';

import './ProductGrid.css';

class ProductGrid extends React.Component {
  render() {
    return (
      <div className='ProductGrid'>
        {/* Remove this line and render products here.*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  addToCart: addProductToCart
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductGrid);
